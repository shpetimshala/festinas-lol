$(function () {
    var loltable = $("#loltable");
    var lols = 0;

    $("body").on("click", ".count-lol", function (e) {
        e.preventDefault();

        bootbox.prompt("Please enter your name!", function (name) {
            if (name != null && name.length > 0) {
                storeLol(name, updateLolTable);
            } else {
                alert("Wont log LOL without name");
            }
        });
    });

    function updateLolTable() {
        lols = 0;
        loltable.find("tbody").empty();
        getLols(function (err, row) {
            lols++;
            var date = new Date(row.timestamp);
            var formattedDate = date.getDate() + "." + date.getMonth() + "." + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes();
            loltable.find("tbody").append("<tr><td>" + formattedDate + "</td><td>" + row.author + "</td></tr>");
        });

        setTimeout(function() {
            if (lols > 0) {
                $("#haslols").show();
                $("#nolols").hide();
            } else {
                $("#haslols").hide();
                $("#nolols").show();
            }

            $("#lolcount").text(lols);
        }, 1000);
    }
    updateLolTable();
});