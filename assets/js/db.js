var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('lol.db');

function initDb() {
    db.serialize(function () {
        db.run(`CREATE TABLE IF NOT EXISTS "lols" (
        "timestamp" integer(90,2),
        "author" text(255),
        PRIMARY KEY ("timestamp")
      );`);
    });
}
initDb();

function storeLol(author, cb) {
    var stmt = db.prepare("INSERT INTO lols VALUES (?, ?)");
    stmt.run(new Date().getTime(), author);
    stmt.finalize();

    cb();
}

function getLols(cb) {
    db.each("SELECT * FROM lols ORDER BY timestamp DESC", cb);
}